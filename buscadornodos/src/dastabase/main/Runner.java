package dastabase.main;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import database.manager.DatabaseManager;
import database.model.TableEdges;
import database.model.TableNodes;
import database.test.DadoUnBaseDeDatosComprobarLaExistenciaDeLasAristas;
import database.test.DadoUnBaseDeDatosComprobarLaExistenciaDeLosNodos;
import database.test.DadoUnBaseDeDatosComprobarSuConectividad;
import database.test.DadoUnNodoComprobarQueNoTienePadre;
import database.test.DadoUnNodoComprobarQueSoloTieneUnHijo;
import database.test.DadoUnNodoHijoComprobarQueNoTieneDependencias;
import database.util.FileWriter;

public class Runner {
	public static DatabaseManager manager;

	private static Class[] funcionalTests = {DadoUnBaseDeDatosComprobarSuConectividad.class,
		DadoUnBaseDeDatosComprobarLaExistenciaDeLosNodos.class,
		DadoUnBaseDeDatosComprobarLaExistenciaDeLasAristas.class};
	
	private static Class[] nodeTests = {
			DadoUnNodoComprobarQueSoloTieneUnHijo.class,
			DadoUnNodoHijoComprobarQueNoTieneDependencias.class,
			DadoUnNodoComprobarQueNoTienePadre.class};
		
	private static String fichero = "output.txt";
	private static boolean funcionaCorrectamente;
	private static FileWriter escritor;
	
	private static ArrayList<String[]> nodosEncontrados;
	
	public static void main(String[] args) throws IOException{
		inicializarParametros();
		testearElFuncionamientoDeLaBaseDeDatos();
		desconectarDeLaBaseDeDatos();
		if(funcionaCorrectamente){
			buscarNodosQueTengaUnUnicoHijoYQueNoTienePadre();
			escribirLosResultadosEnUnFichero();
		}
	}
	
	private static void desconectarDeLaBaseDeDatos(){
		manager.disconnectFromDatabase();
	}
	
	private static void buscarNodosQueTengaUnUnicoHijoYQueNoTienePadre(){
		while(manager.hasNextNode()){
			testearQueElNodoTengaUnUnicoHijoYNoPadre();
		}
	}
	
	
	private static void escribirLosResultadosEnUnFichero() throws IOException{
		escritor.escribirEnElFichero(nodosEncontrados);
		escritor.closedFile();
	}
	
	
	
	private static void testearQueElNodoTengaUnUnicoHijoYNoPadre(){
		boolean[] results = new boolean[nodeTests.length];
		String node =manager.getNextNode();
		aplicarLosTestsSobreElNodo(results);
		if(sonSatisfactoriosLosResultados(results)){
			guardarNodo();
		}
	}
	
	private static void guardarNodo(){
		String[] nodosConexos = new String[2];
		nodosConexos[0]= manager.getCurrentNode();
		nodosConexos[1]=manager.getCurrentNodeDependencies().get(0);
		nodosEncontrados.add(nodosConexos);
	}
	
	private static void aplicarLosTestsSobreElNodo(boolean[] results){
		for(int j=0; j<nodeTests.length; j++){
			Result result = JUnitCore.runClasses(nodeTests[j]);
			results[j]= result.wasSuccessful();
		}
	}
	
	private static void testearElFuncionamientoDeLaBaseDeDatos(){
		boolean[] results = new boolean[funcionalTests.length];
		for(int i=0; i<funcionalTests.length;i++){
			Result result = JUnitCore.runClasses(funcionalTests[i]);
			results[i] = result.wasSuccessful();
		}
		funcionaCorrectamente=sonSatisfactoriosLosResultados(results);
	}
	
	private static void inicializarParametros() throws IOException{
		manager = new DatabaseManager();
		escritor = new FileWriter(fichero);
		nodosEncontrados = new ArrayList<String[]>();
	}
		
	private static boolean sonSatisfactoriosLosResultados(boolean[] results){
		for(boolean satisfactorio: results){
			if(!satisfactorio) return false;
		}
		return true;
	}

}
