package database.model;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private String name;
	private List<String> inputEdges;
	private List<String> outputEdges;
	
	
	public Node(String name){
		this.name = name;
		inputEdges = new ArrayList<String>();
		outputEdges = new ArrayList<String>();
	}
	
	public String getName(){
		return this.name;
	}
	
	public void addFather(String father){
		System.out.println("Null inputEdges? "+inputEdges==null);
		inputEdges.add(father);
	}
	
	public void addDependency(String son){
		System.out.println("Null outputEdges? "+outputEdges==null);
		outputEdges.add(son);
	}
	
	public int getNumberInputEdge() {
		// TODO Auto-generated method stub
		return inputEdges.size();
	}
	
	public int getNumberOutputEdge(){
		return outputEdges.size();
	}
	
	public List<String> getInputEdges(){
		return this.inputEdges;
	}
	
	public List<String> getOutputEdges(){
		return this.outputEdges;
	}
}
