package database.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;

public class TableNodes {
	private ArrayList<String> nodes;
	
	public TableNodes(){
		nodes = new ArrayList<String>();
	}

	public void addNode(String node){
		//System.out.println("Tamnayo actual de la lista(TableNodes): "+nodes.size());
		nodes.add(node);
	}
	
	
	public boolean hasNode(String nodo) {
		// TODO Auto-generated method stub
		return nodes.contains(nodo);
	}
	
	public int getTotalNumberOfNodes(){
		return nodes.size();
	}
	
	
	public String getNodeAtPosition(int x){
		return nodes.get(x);
	}
	
	
	

}
