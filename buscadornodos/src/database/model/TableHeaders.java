package database.model;

import java.util.ArrayList;

public class TableHeaders {
	private String[] cabeceras;
	private int numberOfIndex;
	
	public TableHeaders(int columnCount) {
		cabeceras = new String[columnCount];
		numberOfIndex = 0;
	}

	public int getSize(){
		return cabeceras.length;
	}

	public void addHeader(String columnName) {
		cabeceras[numberOfIndex] = columnName;
		numberOfIndex++;
	}

	public String getHeader(int i) {
		return cabeceras[i];
	}

	public void printHeaders() {
		for(int i=0; i<cabeceras.length; i++){
			System.out.print(cabeceras[i]+", ");
		}
		System.out.print("\n");
	}
}
