package database.model;

import java.util.ArrayList;
import java.util.HashMap;

public class TableEdges {
	private HashMap<String, ArrayList<String>> inputEdges;
	private HashMap<String, ArrayList<String>> outputEdges;
	
	public TableEdges(){
		inputEdges = new HashMap<String, ArrayList<String>>();
		outputEdges = new HashMap<String, ArrayList<String>>();
	}
	
	public void addEdge(String[] arista){
		updateParentNode(arista[2], arista[3]);
		updateChildNode(arista[2], arista[3]);
	}
	
	public int getNumberOfParentsOfNode(String node){
		if(inputEdges.get(node)==null) return 0;
		return inputEdges.get(node).size();
	}
	
	public int getNumberOfDependencies(String nodo){
		if(inputEdges.get(nodo)==null) return 0;
		return(outputEdges.get(nodo).size());
	}
	
	public int getNumberOfEdges(){
		return inputEdges.size();
	}
	
	public ArrayList<String> getDependenciesOfNode(String node){
		if(outputEdges.get(node)==null) return new ArrayList<String>();
		return outputEdges.get(node);
	}
	
	
	private void updateParentNode(String parentNode, String childNode){
		if(outputEdges.containsKey(parentNode)){
			ArrayList<String> dependencies = outputEdges.get(parentNode);
			if(!dependencies.contains(childNode)) dependencies.add(childNode);
		}else{
			ArrayList<String> dependencies = new ArrayList<String>();
			dependencies.add(childNode);
			outputEdges.put(parentNode, dependencies);
		}
	}
	
	private void updateChildNode(String parentNode, String childNode){
		if(inputEdges.containsKey(childNode)){
			ArrayList<String> parents = inputEdges.get(childNode);
			if(!parents.contains(parentNode)) parents.add(parentNode);
		}else{
			ArrayList<String> parents = new ArrayList<String>();
			parents.add(parentNode);
			inputEdges.put(childNode, parents);
		}
	}
}
