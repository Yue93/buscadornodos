package database.manager;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.hamcrest.Matcher;

import database.connection.DatabaseConnector;
import database.model.Node;
import database.model.TableNodes;
import database.request.LoadNodesQuery;
import database.request.ReadQuery;

/**
 * Clase gestor de la base de datos
 * @author yuelin01
 *
 */
public class DatabaseManager {
	// Credenciales para conectar a la base de datos.
	private static DatabaseConnector connector;
	private LoadNodesQuery query;
	private int currentNodeIndex;
	private String currentNode;
	
	public DatabaseManager() throws IOException{
		currentNodeIndex = 0;
		currentNode = null;
		connector = new DatabaseConnector();
	}
	
	/**
	 * Funcion para conectar a la base de datos
	 * Se llama a la funcion connect de la clase DatabaseConnector
	 * @throws IOException 
	 */
	public static void connectToDatabase() throws IOException{
		connector.connect();
	}
	
	/**
	 * Funcion para desconectar de la base de datos.
	 * Se llama a la funcion disconnect de la clase DatabaseConnector 
	 */
	public static void disconnectFromDatabase(){
		connector.disconnect();
	}
	
	public void LoadAllNodes() throws SQLException{
		query = new LoadNodesQuery(connector.getConnection());
		query.loadNodes("DFE.MD_TADM_DEFINICION_NODOS", Arrays.asList("PK_NODO"), "ROWNUM < 300000");
	}

	
	public void LoadAllEdges() throws SQLException{
		//query = new LoadNodesQuery(connector.getConnection());
		query.loadEdges("DFE.MD_TADM_DEFINICION_ARISTAS", Arrays.asList("*"), "ROWNUM < 300000");
	}
	
	public String getNextNode(){
		currentNode = query.getNodeFromTable(currentNodeIndex);
		currentNodeIndex++;
		return currentNode;
	}
	
	public String getCurrentNode(){
		return this.currentNode;
	}
	
	public int getNumberOfDependenciesOfCurrentNodeFirstDependency(){
		if(query.getDependenciesOfNode(currentNode)==null) return 0;
		return query.getDependenciesOfNode(query.getDependenciesOfNode(currentNode).get(0)).size();
	}
	
	public ArrayList<String> getCurrentNodeDependencies(){
		return query.getDependenciesOfNode(currentNode);
	}
	
	public int getCurrentNodeNumberOfParents(){
		return query.getNumberOfParentsOfNode(currentNode);
	}
	
	public boolean hasNextNode(){
		return currentNodeIndex < query.getTotalNumberOfNodes();
	}


	public int getCurrentNodeNumberOfDependecies() {
		return query.getDependenciesOfNode(currentNode).size();
	}
		
	public TableNodes getNodes(){
		return query.getNodes();
	}
	
	public int getNumberOfNodes(){
		return this.query.getTotalNumberOfNodes();
	}
	
	public int getNumberOfEdges(){
		return this.query.getTotalNumberOfEdges();
	}
	
	public boolean estaEstablecidaLaConexion(){
		return (connector.getConnection()!=null);
	}
	
}
