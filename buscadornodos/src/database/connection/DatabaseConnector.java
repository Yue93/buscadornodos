package database.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Clase conector de la base de datos
 * @author yuelin01
 *
 */
public class DatabaseConnector {
	private String user;
	private String database;
	private String password;
	
	private Connection connection;
	private InputStream inputStream;
	private Properties prop;
	
	/**
	 * Constructor de la clase
	 * @param database la base de datos que queremos conectar
	 * @param user el nombre de usuario
	 * @param password la contrasenya de la cuenta
	 * @throws IOException 
	 */
	public DatabaseConnector() throws IOException{
		initializePropertiesFileReader();
		initializeParameters();
	}
	
	
	private void initializePropertiesFileReader() throws IOException{
		prop = new Properties();
		String configFile = "config.properties";
		
		inputStream = getClass().getClassLoader().getResourceAsStream(configFile);
		if(inputStream!=null){
			System.out.println("Properties file loaded!");
			prop.load(inputStream);
		}else{
			throw new FileNotFoundException("property file '" + configFile + "' not found in the classpath");
		}
	}
	
	private void initializeParameters(){
		getUserName();
		getUserPassword();
		getDatabaseLocation();
	}
	
	private void getUserName(){
		this.user = prop.getProperty("user");
	}
	
	private void getUserPassword(){
		this.password = prop.getProperty("password");
	}
	
	private void getDatabaseLocation(){
		this.database = prop.getProperty("databaseLocation");
	}
	
	/**
	 * Funcion que realiza la conexion con la base de datos
	 * Se llama primero a la funcion setDriver para registrar los m�todos de la Oracle sql connector
	 */
	public void connect(){
		setDriver();
		
		try {
			System.out.println("Database connection: "+database);
			connection=DriverManager.getConnection(database, user, password);
			if(connection!=null){
				System.out.println("Successful connection!");
			}else{
				System.out.println("Failed to connect!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("Connection failed! Check out console");
			e.printStackTrace();
			
		}
	}
	
	/**
	 * Funcion que se llama para registrar los m�todos de Oracle sql connector
	 */
	private void setDriver(){
		System.out.println("-------- Oracle JDBC Connection Testing ------");

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return;
		}
		System.out.println("Oracle JDBC Driver Registered!");
	}
	
	/**
	 * Funcion que se llama para cerrar la conexion con la base de datos
	 */
	public void disconnect(){
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("Fail to disconnect from database! Check out console");
			e.printStackTrace();
		}
	}
	
	public Connection getConnection(){
		return this.connection;
	}
}
