package database.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

public class FileWriter {
	private Writer writer;
	
	public FileWriter(String fichero) throws FileNotFoundException{
		FileOutputStream fileOutput = new FileOutputStream(new File(fichero));
		OutputStreamWriter outputWriter = new OutputStreamWriter(fileOutput);
		writer= new BufferedWriter(outputWriter);
	}
	
	
	
	public void closedFile() throws IOException{
		writer.close();
	}
	
	public void escribirEnElFichero(ArrayList<String[]> arraylist) throws IOException{
		String contenidoLinea ="";
		for(String[] nodos: arraylist){
			contenidoLinea=nodos[0]+" depende de "+nodos[1]+"\n";
			writer.write(contenidoLinea);
		}
	}
}
