package database.request;

import java.sql.Connection;
import java.util.List;


public class Query {
	protected Connection connection;
	protected String table;
	protected List<String> columnas;
	protected String condition;
	
	protected String query;
	
	public Query(Connection connection){
		this.connection=connection;
	}

	public Query(Connection connection, String table, List<String> columnas) {
		this.connection=connection;
		this.table=table;
		this.columnas=columnas;
	}

	public Query(Connection connection, String table, List<String> columnas,
			String condition) {
		this.connection=connection;
		this.table=table;
		this.columnas=columnas;
		this.condition=condition;
	}
}
