package database.request;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import database.model.TableHeaders;
/**
 * Clase para realizar las queries de lectura
 * @author yuelin01
 *
 */
public class ReadQuery extends Query{
	protected String table;
	protected Statement statement;
	protected ResultSet result;
	protected TableHeaders headers;
	
	/**
	 * Constructor de la clase
	 * @param connection
	 */
	public ReadQuery(Connection connection){
		super(connection);
	}
	
	/**
	 * Constructor de la clase
	 * @param connection
	 * @param table
	 * @param columnas
	 */
	public ReadQuery(Connection connection, String table, List<String> columnas){
		super(connection, table, columnas);
	}
	
	/**
	 * Constructor de la clase
	 * @param connection
	 * @param table
	 * @param columnas
	 * @param condition
	 */
	public ReadQuery(Connection connection, String table, List<String> columnas, String condition){
		super(connection, table, columnas, condition);
	}
	
	/**
	 * M�todo para generar la query completa
	 */
	protected void createQuery(){
		query = "select ";
		for(String str: columnas){
			query = query + str + ",";
		}
		query = query.substring(0, query.length()-1);
		query = query+ " from " + this.table + " WHERE "+this.condition;
		
	}
	
	/**
	 * M�todo para ejecutar la query de lectura
	 * @param table
	 * @param columnas
	 * @param condition
	 */
	public void readTable(String table, List<String> columnas, String condition){
		try {
			setParametros(table, columnas, condition);
			createQuery();
			System.out.println("Query: "+query);
			result = statement.executeQuery(query);
			saveHeaders();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setParametros(String table, List<String> columnas, String condition) throws SQLException{
		this.table=table;
		this.columnas= columnas;
		this.condition=condition;
		statement = connection.createStatement();
	}
	
	/**
	 * M�todo para guardar las cabeceras de la tabla
	 * @throws SQLException 
	 */
	protected void saveHeaders() throws SQLException{
		ResultSetMetaData metadata;
		metadata = result.getMetaData();
		headers = new TableHeaders(metadata.getColumnCount());
		System.out.println("Numero de columnas "+metadata.getColumnCount());
		for(int i=1; i <= metadata.getColumnCount(); i++){
			headers.addHeader(metadata.getColumnName(i));
		}
		
	}
	
	/**
	 * M�todo para imprimir los n primeros elementos de la tabla
	 * @param n
	 * @throws SQLException 
	 */
	public void printNRows(int n) throws SQLException{
		int nread = 0;
		while(result.next() && nread<n){
			printRow();
			nread+=1;
		}
	}
	
	/**
	 * M�todo para imprimir los n primeros elementos de la tabla
	 * @param n
	 * @throws SQLException 
	 */
	public void printAllRows() throws SQLException{
		while(result.next()) printRow();
		System.out.println("En total hay "+headers.getSize()+" ");
	}
	
	private void printRow() throws SQLException{
		for(int i = 0; i < headers.getSize(); i++){
			//System.out.println("Columna: "+headers.getHeader(i)+ " "+i);
			String cell = result.getString(headers.getHeader(i));
			System.out.print(cell+" ");
		}
		System.out.print("\n");
	}
	
	/**
	 * Metodo para imprimir las cabeceras de la tabla
	 */
	public void printHeader(){
		headers.printHeaders();
	}
	
	public TableHeaders getHeaders(){
		return this.headers;
	}
}
