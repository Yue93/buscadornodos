package database.request;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.model.Node;
import database.model.TableEdges;
import database.model.TableNodes;

public class LoadNodesQuery extends ReadQuery{
	TableNodes nodos;
	TableEdges aristas;
	
	public LoadNodesQuery(Connection connection){
		super(connection);
		nodos = new TableNodes();
		aristas = new TableEdges();
	}
	
	public LoadNodesQuery(Connection connection, String table, List<String> columnas, String condition) {
		super(connection, table, columnas, condition);
		nodos = new TableNodes();
		aristas = new TableEdges();
		// TODO Auto-generated constructor stub
	}
	
	public void loadNodes(String table, List<String> columns, String condition) throws SQLException{
		setParameters(table, columns, condition);
		createQuery();
		getQueryResult();
		saveHeaders();
		saveNodes();
	}
	
	
	public void loadEdges(String table, List<String> columns, String condition) throws SQLException {
		// TODO Auto-generated method stub
		setParameters(table, columns, condition);
		createQuery();
		getQueryResult();
		saveHeaders();
		updateNodeDependencies();
	}
	
	public TableNodes getNodes(){
		return this.nodos;
	}
	
	public String getNodeFromTable(int x){
		return nodos.getNodeAtPosition(x);
	}
	
	public int getTotalNumberOfNodes(){
		return this.nodos.getTotalNumberOfNodes();
	}
	
	public int getTotalNumberOfEdges(){
		return aristas.getNumberOfEdges();
	}
	
	public ArrayList<String> getDependenciesOfNode(String node){
		return aristas.getDependenciesOfNode(node);
	}
	
	public int getNumberOfParentsOfNode(String node){
		return aristas.getNumberOfParentsOfNode(node);
	}
	
	private void updateNodeDependencies() throws SQLException{
		String cell;
		while(result.next()){
			String[] arista = new String[headers.getSize()];
			for(int i = 0; i < headers.getSize(); i++){
				cell = result.getString(headers.getHeader(i));
				arista[i]=cell;
			}
			aristas.addEdge(arista);
		}
	}
	
	
	private void getQueryResult() throws SQLException{
		statement = connection.createStatement();
		result = statement.executeQuery(super.query);
	}
	
	private void setParameters(String table, List<String> columns, String condition){
		super.table = table;
		super.columnas = columns;
		super.condition = condition;
	}
	
	private void saveNodes() throws SQLException{
		String cell;
		while(result.next()){
			for(int i = 0; i < headers.getSize(); i++){
				cell = result.getString(headers.getHeader(i));
				nodos.addNode(cell);
			}
		}
	}

}
