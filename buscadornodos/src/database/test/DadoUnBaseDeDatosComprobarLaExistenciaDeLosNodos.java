package database.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dastabase.main.Runner;
import database.connection.DatabaseConnector;
import database.manager.DatabaseManager;


public class DadoUnBaseDeDatosComprobarLaExistenciaDeLosNodos {

	private DatabaseManager manager;	
	
	@Before
	public void setUp() throws IOException, SQLException{
		manager = Runner.manager;
		manager.LoadAllNodes();
	}
	
	private boolean elNumeroDeNodoEsMasGrandeQueCero(int n){
		return n>0;
	}
	
	@Test
	public void test() {
		Assert.assertTrue(elNumeroDeNodoEsMasGrandeQueCero(manager.getNumberOfNodes()));
	}
}
