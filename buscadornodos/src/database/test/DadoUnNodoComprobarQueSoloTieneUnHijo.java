package database.test;

import static org.junit.Assert.*;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import dastabase.main.Runner;
import database.manager.DatabaseManager;

public class DadoUnNodoComprobarQueSoloTieneUnHijo {
	@Rule public TestName name = new TestName();
	
	private DatabaseManager manager;	
	private String node;
	
	@Before
	public void setUp() throws IOException{
		manager = Runner.manager;
		node = manager.getCurrentNode();
	}
	
	@Test
	public void test() {
		Assert.assertEquals(1, manager.getCurrentNodeNumberOfDependecies());
	}

}
