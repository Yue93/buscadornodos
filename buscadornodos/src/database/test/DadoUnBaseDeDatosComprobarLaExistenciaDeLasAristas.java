package database.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dastabase.main.Runner;
import database.manager.DatabaseManager;

public class DadoUnBaseDeDatosComprobarLaExistenciaDeLasAristas {

private DatabaseManager manager;	
	
	@Before
	public void setUp() throws IOException, SQLException{
		manager = Runner.manager;
		manager.LoadAllEdges();
	}
	
	private boolean elNumeroDeAristasEsMasGrandeQueCero(int n){
		return n>0;
	}
	
	@Test
	public void test() {
		Assert.assertTrue(elNumeroDeAristasEsMasGrandeQueCero(manager.getNumberOfEdges()));
	}

}
