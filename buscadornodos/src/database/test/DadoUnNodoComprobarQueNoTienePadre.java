package database.test;

import static org.junit.Assert.*;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import dastabase.main.Runner;
import database.manager.DatabaseManager;

public class DadoUnNodoComprobarQueNoTienePadre {

	private DatabaseManager manager;	
	
	@Before
	public void setUp() throws IOException{
		manager = Runner.manager;
	}
	
	@Test
	public void test() {
		Assert.assertEquals(0, manager.getCurrentNodeNumberOfParents());
	}

}
