package database.test;

import static org.junit.Assert.*;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dastabase.main.Runner;
import database.connection.DatabaseConnector;
import database.manager.DatabaseManager;

public class DadoUnBaseDeDatosComprobarSuConectividad {
	private DatabaseManager manager;
	
	@Before
	public void setUp() throws IOException{
		manager = Runner.manager;
		manager.connectToDatabase();
	}
	
	@Test
	public void test() {
		Assert.assertTrue(manager.estaEstablecidaLaConexion());
	}

}
